import { Component, OnInit } from '@angular/core';

import {AccountsService} from './accounts.service'

@Component({
  selector: 'prac-services',
  templateUrl: './prac-services.component.html',
  styleUrls: ['./prac-services.component.css']
})
export class PracServicesComponent implements OnInit {
  accounts:{name:string,status:string}[]
  constructor(private accountsData:AccountsService) { }

  ngOnInit(): void {
    this.accounts = this.accountsData.accounts
  }

  // accounts = [
  //   {
  //     name: 'Master Account',
  //     status: 'active'
  //   },
  //   {
  //     name: 'Testaccount',
  //     status: 'inactive'
  //   },
  //   {
  //     name: 'Hidden Account',
  //     status: 'unknown'
  //   }
  // ];

  // onAccountAdded(newAccount: {name: string, status: string}) {
  //   this.accounts.push(newAccount);
  // }

  // onStatusChanged(updateInfo: {id: number, newStatus: string}) {
  //   this.accounts[updateInfo.id].status = updateInfo.newStatus;
  // }

}
