import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'prac-directive',
  templateUrl: './prac-directive.component.html',
  styleUrls: ['./prac-directive.component.css']
})
export class PracDirectiveComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  // numbers = [1, 2, 3, 4, 5];
  onlyOdd = false;
  evenNumbers = [2,4]
  oddNumbers=[1,3,5]
  value:number = 5
}
