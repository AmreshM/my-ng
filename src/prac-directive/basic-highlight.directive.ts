import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector:"[basicHighlight]"
})
export class BasicHighlightDirective implements OnInit{
    constructor(private createElement:ElementRef){}

    ngOnInit(){
        this.createElement.nativeElement.style.backgroundColor = 'green'
        // this is not the best way to hit the DOM directly and change
    }
}