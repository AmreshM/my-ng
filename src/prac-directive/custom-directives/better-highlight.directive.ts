import { 
  Directive, 
  ElementRef, 
  Renderer2, 
  OnInit, 
  HostListener, 
  HostBinding, 
  Input

} from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  // @Input() defaultColor:string = 'blue'
  @Input('appBetterHighlight') defaultColor:string = 'blue'
  @Input() highlightColor:string

  @HostBinding('style.backgroundColor') bgColor:string = 'transparent'
  constructor(private createEl:ElementRef, private renderer:Renderer2) { }

  ngOnInit(){
    // this.renderer.setStyle(this.createEl.nativeElement,'background-color','blue')
    // better way because we use Angular in some places where no concept of DOM.

    this.bgColor= this.defaultColor;
  }
  
  @HostListener('mouseenter') mouseEnter (eventData:Event){
    // this.renderer.setStyle(this.createEl.nativeElement,'background-color','blue')
    // this.bgColor = 'blue'
    this.bgColor= this.highlightColor;
  }
  @HostListener('mouseleave') mouseLeave (eventData:Event){
    // this.renderer.setStyle(this.createEl.nativeElement,'background-color','transparent')
    // this.bgColor = 'transparent'
    this.bgColor= this.defaultColor;
  }

}
