import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { PracDirectiveComponent } from './prac-directive.component';
import {BasicHighlightDirective} from './basic-highlight.directive'
import {BetterHighlightDirective} from './custom-directives/better-highlight.directive'
import { UnlessDirective } from './custom-directives/unless.directive';
@NgModule({
  declarations: [
    PracDirectiveComponent,
    BasicHighlightDirective,
    BetterHighlightDirective,
    UnlessDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [PracDirectiveComponent]
})
export class PracDirectiveModule { }