import { EventEmitter, Injectable } from '@angular/core';

import {Ingredient} from '../shared/ingredient.model'
import {Recipe} from "./recipe.model";

import { ShoppingListService } from './../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {

selectedRecipe=new EventEmitter<Recipe>();

private recipes:Recipe[]=[
    new Recipe(
      'Test Recipe',
      'This is description',
      'https://natashaskitchen.com/wp-content/uploads/2019/04/Best-Burger-5-600x900.jpg',
      [new Ingredient("Apple",5),new Ingredient("Orange",10)]
      ),    
    new Recipe(
      'Burger Recipe',
      'This is description',
      'https://natashaskitchen.com/wp-content/uploads/2019/04/Best-Burger-5-600x900.jpg',
      [new Ingredient("Onion",1),new Ingredient("tomato",1)]
      ),    
  ] 


  constructor(private slService:ShoppingListService){}

  getRecipe(){
      //give the new copie of the array not the reference of the same for that used slice()
      return this.recipes.slice()
  }

  addIngredientToShoppingList(ingredient:Ingredient[]){
    this.slService.addIngredients(ingredient)
  }



}