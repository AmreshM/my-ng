import {EventEmitter} from '@angular/core'
import {Ingredient} from '../shared/ingredient.model'

export class ShoppingListService {

    // onAddIngredient = new EventEmitter<Ingredient>()
    ingredientsChanged = new EventEmitter<Ingredient[]>()
    
   private ingredients:Ingredient[] = [
        new Ingredient("Apple",5),
        new Ingredient("Orange",10)
      ];
    // private ingredients:Ingredient[]
    getIngredients(){
        return this.ingredients.slice()
    }

    addIngredient(ingredient:Ingredient){
        this.ingredients.push(ingredient)
        this.ingredientsChanged.emit(this.ingredients.slice())//pass a copy of it
    }
    
    addIngredients(ingredient:Ingredient[]){
        // for (let index = 0; index < ingredient.length; index++) {
        //     this.ingredients.push(ingredient[index])     //unnecessary event emits  
        // }

        this.ingredients.push(...ingredient)
        this.ingredientsChanged.emit(this.ingredients.slice())//to pass a new copy of it for that used emit and slice method
    }
}