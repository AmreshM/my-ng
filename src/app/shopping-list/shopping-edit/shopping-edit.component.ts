import { 
  Component, 
  OnInit, 
  ViewChild, 
  ElementRef, 
  // Output, 
  // EventEmitter 
} from '@angular/core';

import {ShoppingListService} from '../shopping-list.service';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  @ViewChild('nameInput',{static:false}) inputName:ElementRef
  @ViewChild('numberInput',{static:false}) inputNumber:ElementRef
  // @Output() ingredientEmitter = new EventEmitter<Ingredient>()
  constructor(private shoppingListService:ShoppingListService) { }

  ngOnInit(): void {
  }
 
  onAddClick(){
    // this.ingredientEmitter.emit(new Ingredient(this.inputName.nativeElement.value,this.inputNumber.nativeElement.value))
    // this.shoppingListService.onAddIngredient.emit(new Ingredient(this.inputName.nativeElement.value,this.inputNumber.nativeElement.value))
    this.shoppingListService.addIngredient(new Ingredient(this.inputName.nativeElement.value,this.inputNumber.nativeElement.value))
  }

  onDeleteClick(){

  }

  onClearClick(){

  }
}
