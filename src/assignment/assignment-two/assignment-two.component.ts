import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'assignment-two',
  templateUrl: './assignment-two.component.html',
  styleUrls: ['./assignment-two.component.css']
})
export class AssignmentTwoComponent implements OnInit {

  isDisplay = true;
  counter=0;
  timeStamp = null;
  counterArray= [];
  
  constructor() { }

  ngOnInit(): void {
  }

  toggleDisplay(){
    
      this.isDisplay = !this.isDisplay
      // this.counterArray.push(this.counter++)
      this.counterArray.push(new Date)
      
  }
}
