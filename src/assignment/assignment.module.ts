import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';


import { AssignmentComponent } from './assignment.component';

import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import {AssignmentBindingComponent} from './assignment-binding/assignment-binding.component';
import {AssignmentTwoComponent} from './assignment-two/assignment-two.component';
import { GamecontrolComponent } from './gamecontrol/gamecontrol.component';
import { GameOddComponent } from './gamecontrol/game-odd/game-odd.component';
import { GameEvenComponent } from './gamecontrol/game-even/game-even.component';
@NgModule({
  declarations: [
    AssignmentComponent,
    SuccessAlertComponent,
    WarningAlertComponent,
    AssignmentBindingComponent,
    AssignmentTwoComponent,
    GamecontrolComponent,
    GameOddComponent,
    GameEvenComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AssignmentComponent]
})
export class AssignmentModule { }
