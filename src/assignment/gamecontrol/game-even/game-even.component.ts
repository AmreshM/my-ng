import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'assignment-game-even',
  templateUrl: './game-even.component.html',
  styleUrls: ['./game-even.component.css']
})
export class GameEvenComponent implements OnInit {

  @Input('evenNumber') number:number
  constructor() { }

  ngOnInit(): void {
  }

}
