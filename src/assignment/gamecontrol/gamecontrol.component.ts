import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'assignment-gamecontrol',
  templateUrl: './gamecontrol.component.html',
  styleUrls: ['./gamecontrol.component.css']
})
export class GamecontrolComponent implements OnInit {

  @Output() intervalFired=new EventEmitter<number>()
  interval;
  count = 0;
  constructor() { }

  ngOnInit(): void {
  }

  onStartClick(){    
    this.interval = setInterval(()=>{
      this.intervalFired.emit(this.count+1)
      this.count++
    },1000)
  }
  onStopClick(){
    clearInterval(this.interval);
  }
}
