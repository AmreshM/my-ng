import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'practice-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  btnDisable=false;
  serverCreated = "server was not created."
  serverName = ''
  
  constructor() { 
    setTimeout(()=>{
      this.btnDisable = true
    },2000)
  }

  handleClick(){
    this.serverCreated="server was created and server name is" + this.serverName
  }
  handleInput (event:Event) {
    console.log(event)
    this.serverName = (<HTMLInputElement>event.target).value
  }
  ngOnInit(): void {
  }

  

}
