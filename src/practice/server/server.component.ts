import { Component } from '@angular/core';


@Component({
    selector:'practice-server', // Element selector //custom html tag created that should be unique
    // selector same as css selector but id selctor and sudo selector don't work
    // attribute selector [practice-server]
    // class selector '.practice-server'
    templateUrl:'./server.component.html', // path to show html content or external template
    // template:`
    // <p>
    // <practice-servers></practice-servers>
    // </p>`
    styleUrls:['./server.component.css'], //contain array of css path override the next index path to previous one if same class used. menas next index priority first
    styles:[],//array of strings of css and inline styling priority first

}) //decorators are typescript feature which enhance your class feature

export class ServerComponent {

    serverId = 10;
    serverStatus = 'offline'
    isServer = false;
    serversArray=[];
    serverDesc = "server was created"
    constructor (){
        this.serverStatus = Math.random() >0.5 ? "online":"offline"
    }
    getServerStatus(){
        // return this.serverStatus
        // return {
        //     id:this.serverId,
        //     status:this.serverStatus
        // }
    }

    serverCreated (){
        this.isServer = true
    }

    changeColor(){
        return this.serverStatus === "online" ? 'green' : "red"
    }
    handleAdd(){
        this.serversArray.push(this.serverDesc)
        console.log(this.serversArray)
      }
}

//Need to have a template or templateurl otherwise we cannot build any component
// error NG6001: The class 'ServerComponent' is listed in the declarations of the NgModule 'PracticeModule', but is not a
// directive, a component, or a pipe. Either remove it from the NgModule's declaration