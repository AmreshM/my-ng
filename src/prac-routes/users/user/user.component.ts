import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit,OnDestroy {
  user: {id: number, name: string};

  paramsSubscription:Subscription
  constructor(
    private activateRoute:ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit() {
    // this.user = {
    //   id:this.activateRoute.snapshot.params["id"],
    //   name:this.activateRoute.snapshot.params["name"]
    // }
    this.activateRoute.params
    .subscribe(
      (params:Params)=>{
        this.user = {
          id:params['id'],
          name:params['name']
        }
      }
    )

    // No need to unsbscribe because it is handled by Angular here for activateRoute
    // this.paramsSubscription = this.activateRoute.params
    // .subscribe(
    //   (params:Params)=>{
    //     this.user = {
    //       id:params['id'],
    //       name:params['name']
    //     }
    //   }
    // )


  }

  // changeParams(){
  //   this.router.navigate(['/users/2/amit'])
  // }

  ngOnDestroy(){
    // this.paramsSubscription.unsubscribe()
  }

}
