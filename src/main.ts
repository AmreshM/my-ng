import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
// import {PracticeModule} from './practice/practice.module';
// import { AssignmentModule } from './assignment/assignment.module';
// import {PracticeModule} from './practice-databinding/practice-databinding.module';
// import { PracDirectiveModule } from './prac-directive/prac-directive.module'
// import {PracServiceModule} from './prac-services/prac-services.module';
// import {PracticeRoutesModule} from './prac-routes/prac-routes.modules';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
// platformBrowserDynamic().bootstrapModule(PracticeModule)
// platformBrowserDynamic().bootstrapModule(PracticeModule)
// platformBrowserDynamic().bootstrapModule(AssignmentModule)
// platformBrowserDynamic().bootstrapModule(PracDirectiveModule)
// platformBrowserDynamic().bootstrapModule(PracServiceModule)
// platformBrowserDynamic().bootstrapModule(PracticeRoutesModule)
  .catch(err => console.error(err));
