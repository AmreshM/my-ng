import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef} from '@angular/core';

@Component({
  selector: 'practice-databinding-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
 @Output() serverCreated = new EventEmitter<{serverName:string,serverContent:string}>()
 @Output('bpCreated') blueprintCreated = new EventEmitter<{serverName:string,serverContent:string}>()
  // newServerName = '';
  // newServerContent = '';
  @ViewChild('serverContentInput') contentInput:ElementRef
  constructor() { }

  ngOnInit(): void {
  }
  onAddServer(nameInput:HTMLInputElement) {    
    // this.serverCreated.emit({serverName:this.newServerName,serverContent:this.newServerContent})
    this.serverCreated.emit({serverName:nameInput.value,serverContent:this.contentInput.nativeElement.value})
  }

  onAddBlueprint(nameInput:HTMLInputElement) {
    // this.blueprintCreated.emit({serverName:this.newServerName,serverContent:this.newServerContent})
    this.blueprintCreated.emit({serverName:nameInput.value,serverContent:this.contentInput.nativeElement.value})
  }
}
