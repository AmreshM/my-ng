import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'practice-databinding',
  templateUrl: './practice-databinding.component.html',
  styleUrls: ['./practice-databinding.component.css']
})
export class PracticeDatabindingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  serverElements = [{type:'server',name:'testserver',content:'just a test'}];
  onServerAdded(serverData:{serverName:string,serverContent:string}) {
    this.serverElements.push({
      type: 'server',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }

  onBlueprintAdded(blueprintData:{serverName:string,serverContent:string}) {
    this.serverElements.push({
      type: 'blueprint',
      name: blueprintData.serverName,
      content: blueprintData.serverContent
    });
  }

  onChangeClick(){
    this.serverElements[0].name = "changed"
  }

  onDestroyFirst(){
    this.serverElements.splice(0,1)
  }
}
