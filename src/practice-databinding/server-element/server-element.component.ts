import { 
  Component, 
  OnInit, 
  Input, 
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  ContentChild, 
  // OnChanges, 
  // SimpleChanges,
  // DoCheck,
  // AfterContentInit,
  // AfterContentChecked,
  // AfterViewInit,
  // AfterViewChecked,
  // OnDestroy
} from '@angular/core';

@Component({
  selector: 'practice-databinding-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation:ViewEncapsulation.Emulated //None,Native
})
export class ServerElementComponent implements 
OnInit
// OnChanges,
// DoCheck,
// AfterContentInit,
// AfterContentChecked,
// AfterViewInit,
// AfterViewChecked,
// OnDestroy

{

  @Input('srvElement') element:{type:string,name:string,content:string};
  @Input() name:string;
  @ViewChild('heading',{static:true}) header:ElementRef;
  @ContentChild('contentParagraph',{static:true}) paragraph:ElementRef

  // Life cycle methods
  // LifeCycles methods works without importing and implenting but it good practice to implent
  constructor() { 
    // console.log('constructor')
  }
//   ngOnChanges(changes:SimpleChanges){
//     // called when the value changes not refernce
//     console.log(changes) 
//     console.log('ngOnChanges')
// }
  ngOnInit(): void {
    // console.log('ngOnInit')
    // console.log('Text Header:' + this.header.nativeElement.textContent)
    // console.log('Text Header:' + this.paragraph.nativeElement.textContent)
  }

//   ngDoCheck(){
//     console.log('DoCheck')
//   }

  // ngAfterContentInit(){
  //   console.log('ngAfterContentInit')
  //   console.log('Text Header:' + this.paragraph.nativeElement.textContent)
  // }

//   ngAfterContentChecked(){
//     console.log('ngAfterContentChecked')
//   }
  // ngAfterViewInit(){
  //   console.log('ngAfterViewInit')
  //   console.log('Text Header:' + this.header.nativeElement.textContent)
  // }

//   ngAfterViewChecked(){
//     console.log('ngAfterViewChecked')
//   }

//   ngOnDestroy(){
//     console.log('ngOnDestroy')
//   }

}
