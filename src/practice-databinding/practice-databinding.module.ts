import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { PracticeDatabindingComponent } from './practice-databinding.component';
import { CockpitComponent } from './cockpit/cockpit.component';
import { ServerElementComponent } from './server-element/server-element.component';
@NgModule({
  declarations: [
    PracticeDatabindingComponent,
    CockpitComponent,
    ServerElementComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [PracticeDatabindingComponent]
})
export class PracticeModule { }
